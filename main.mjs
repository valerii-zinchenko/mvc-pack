/*
 * mvc-pack
 * Copyright (c) 2016-2020 Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/mvc-pack/-/blob/master/LICENSE.txt)
 * All source files are available at: https://gitlab.com/valerii-zinchenko/mvc-pack
 */

const _register = new WeakMap();

/**
 * Pack MVC components. It registers the components and returns a [function]{@link createView} to create a specific view for a specific model.
 *
 * @example <caption>Basic usage with standard views and controllers</caption>
 * mvcPack(
 * 	SomeModelClass,
 * 	{
 * 		text: vcPack(TextView),
 * 		table: vcPack(TableView, TableController)
 * 	}
 * );
 * const model = new SomeModelClass();
 * console.log(getView(model, 'table') instanceof TableView);	// -> true
 *
 * @example <caption>Creating an MVC pack with non-standard view constructor</caption>
 * mvcPack(
 * 	SomeModelClass,
 * 	{
 * 		nonStandardConstructors(model) {
 * 			return new NonStandardView(model, 'some required string');
 * 		}
 * 	}
 * );
 *
 * @see {@link vcPack} {@link getView}
 *
 * @param {class} Model - A model class.
 * @param {Object} VCPacks - A key-value pair of View-Controller packs. They can be created with help of {@link vcPack} or a custom creator function can be provided.
 */
export function mvcPack(Model, VCPacks) {
	_register.set(Model, VCPacks);
}

/**
 * Get a specific view for a model from a registered pack.
 *
 * The selection is based on the constructor of a model. This gets the registered {@link ViewBuilder} function and passes the needed arguments to it.
 *
 * @param {Object} model - An instance of the model, for what a specific view is needed.
 * @param {String} name - View name (a key in a provided VCPacks object).
 * @param {Array} [viewArgs] - Arguments for a view constructor.
 * @param {Array} [controllerArgs] - Arguments for a controller constructor.
 * @return {Object | null} A new view instance or null.
 */
export function getView(model, name, viewArgs, controllerArgs) {
	const packs = _register.get(model.constructor);
	if (!packs) {
		return null;
	}

	const pack = packs[name];
	return pack ? pack(model, viewArgs, controllerArgs) : null;
}

/**
 * Combines standard view and controller classes for an mvcPack.
 *
 * @example
 * vcPack(SomeViewClass)
 *
 * @example
 * vcPack(SomeViewClass, SomeControllerClass)
 *
 * @see {@link mvcPack}
 *
 * @param {StandardView} View - A view class.
 * @param {StandardController} [Controller] - A controller class.
 * @return {ViewBuilder} A function that automatically creates an instance of a view with a controller.
 */
export function vcPack(View, Controller) {
	return (model, viewArgs = [], controllerArgs = []) => {
		return Controller
			?  new View(
				model,
				Controller ? new Controller(model, ...controllerArgs) : undefined,
				...viewArgs
			)
			: new View(
				model,
				...viewArgs
			);
	};
}

/**
 * This is an example of how a standard view class constructor should look like.
 *
 * @class StandardView
 *
 * @see {@link View}
 *
 * @param {*} model - A model.
 * @param {*} [controller] - A controller for a view.
 */

/**
 * This is an example of how a standard controller class constructor should look like.
 *
 * @class StandardController
 *
 * @see {@link Controller}
 *
 * @param {*} model - A model.
 */

/**
 * Function that builds a view.
 *
 * If a controller constructor was not packed, then the additional arguments for a view will be passer right from the second argument.
 *
 * @function ViewBuilder
 * @param {*} model - An instance of a model.
 * @param {Array} viewArgs - Additional arguments for a view contructor.
 * @param {Array} controllerArgs - Additional arguments for a controller contructor.
 * @return {object} A new instance of a view.
 */
