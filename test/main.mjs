/* eslint-disable max-nested-callbacks, no-magic-numbers */

import {mvcPack, vcPack, getView} from '../main.mjs';

import chai from 'chai';
const assert = chai.assert;


suite('Packs', () => {
	class Model {}
	class View {
		constructor(model, controller, ...args) {
			this.model = model;
			this.controller = controller;
			this.args = args;
		}
	}
	class Controller {
		constructor(model, ...args) {
			this.model = model;
			this.args = args;
		}
	}
	class ViewWithoutController {
		constructor(model, ...args) {
			this.model = model;
			this.args = args;
		}
	}

	suite('mvcPack', () => {
		let model;
		setup(() => {
			model = new Model();
		});

		test('create a view for a model', () => {
			mvcPack(Model, {
				view: vcPack(View)
			});

			assert.instanceOf(getView(model, 'view'), View);
		});

		test('return null for a model if a view key is not exist', () => {
			mvcPack(Model, {
				view: vcPack(View)
			});

			assert.isNull(getView(model, 'nothing'));
		});

		test('return null if a pack is not registered for a model', () => {
			mvcPack(Model, {
				view: vcPack(View)
			});

			assert.isNull(getView({}, 'view'));
		});
	});

	suite('vcPack', () => {
		const model = new Model();

		suite('without a controller constructor', () => {
			test('returns a function that creates a view and sends a model into a view constructor', () => {
				const uut = vcPack(ViewWithoutController)(model);

				assert.instanceOf(uut, ViewWithoutController);
				assert.equal(uut.model, model);
				assert.isUndefined(uut.controller);
			});

			test('returns a function that is passing additional arguments', () => {
				const args = [1, 'str', undefined];
				const uut = vcPack(ViewWithoutController)(model, args);

				assert.instanceOf(uut, ViewWithoutController);
				assert.equal(uut.model, model);
				assert.isUndefined(uut.controller);
				assert.deepEqual(uut.args, args);
			});
		});

		suite('with a controller constructor', () => {
			test('returns a function that creates a view with a controller and sends a model into a view and controller constructor', () => {
				const uut = vcPack(View, Controller)(model);

				assert.instanceOf(uut, View);
				assert.equal(uut.model, model);
				assert.instanceOf(uut.controller, Controller);
				assert.equal(uut.controller.model, model);
			});

			test('returns a function that is passing additional arguments', () => {
				const viewArgs = [1, 'str', undefined];
				const controllerArgs = [false, null, {}];
				const uut = vcPack(View, Controller)(model, viewArgs, controllerArgs);

				assert.instanceOf(uut, View);
				assert.equal(uut.model, model);
				assert.instanceOf(uut.controller, Controller);
				assert.deepEqual(uut.args, viewArgs);
				assert.deepEqual(uut.controller.args, controllerArgs);
			});
		});
	});
});

