# mvc-pack

The main idea is to help to organize models, views and controllers and to have a single point, where all the components are combined together. This could be treated as a global register of view mapped to a model class. This reduces the interdependency of some simple view in contrast to external views, but it then forcing the need oto manage this maps.

## Instalation

```
npm install mvc-pack
```

## Usage

A recommended rules to follow to create a flexible MVC package:

- a model must not care about the way how it can be represented, i.e. a model must not know anything about the related views and controllers
- a model can create another needed models, but view/controller must not create models
- a model does not create views, but a view can create another needed views
- a view and a controller are constructing by taking only a reference to a model as an input argument
- a view and/or a controller listen for a model update events to properly represent the information
- a view is updating only the part that is changed in the model, i.e. a view must not be fully rerendered, only some part of it must be updated
- a view is listening user interaction events and updating the model data over the direct dedicated model API or with help of a controller
- a controller can act as an adapter between the model and the view events, in order to keep raw data in a model and to keep view clean from data processors

Views are providing on demand by an application and the package wrapper is passing a reference to a model as an input argument.

```js
mvcPack(
	SomeModelClass,
	{
		text: vcPack(TextView),
		table: vcPack(TableView, TableController)
	}
);

const model = new SomeModelClass();

console.log(getView(model, 'table') instanceof TableView);   // -> true
```

It is also possible to register view by providing own custom function for that. The only thing to keep in mind, that that function must return a view:

```js
class CustomView {
	...
}

class CustomController {
	...
}

mvcPack(
	SomeModelClass,
	{
		customView(model) {
			const view = new CustomView(model, 'other arguments');
			const controller = new CustomController(model, 'other arguments');
			view.setController(controller);
			return view;
		}
	}
);
```

### Model

It is recommended to create a base model class in scope of an application, in order to attach an event system to trigger and receive events. For example when some property is changing - update all connected views, or when a model is destructing - properly clean up the views.

```js
class Model {
	constructor() {
		this._eventSystem = new EventTarget();

		this.data = {};
	}

	destruct() {
		this.data = null;
		this.dispatchEvent('destruct');
	}

	setData(data) {
		for (let key in data) {
			this.data[key] = delta[key];
			this.dispatchEvent(`update-${key}`);
		}

		this.dispatchEvent('update');
	}

	dispatchEvent(type) {
		this._eventSystem.dispatchEvent(new CustomEvent(type));
	}

	addEventListener(type, listener) {
		this._eventSystem.addEventListener(type, listener);
	}

	removeEventListener(type, listener) {
		this._eventSystem.removeEventListener(type, listener);
	}
}
```


### View

In context of this package a "View" is a final chain between a model and the information representation. A view can be an HTML representation, so a View class in this context is a class that, for example creates all the needed HTML elements and or combines other Views to display some information.

An example to assotiate only a view with a model:

```js
class TextView {
	...
}

class TableView {
	...
}

mvcPack(
	SomeModelClass,
	{
		text: vcPack(TextView),
		table: vcPack(TableView)
	}
);
```

It also can work with other classes, that needs more arguments to be constructed or need to do some extra build steps:

```js
class YourView {
	...
}

mvcPack(
	SomeModelClass,
	{
		customView(model) {
			return new YourView(model, 'other', 'arguments');
		}
	}
);
```


### Controller

Sometimes in between the model and view an additional data processing is needed (for example an aggregation, or an adaptation, or formatting, or something else), but the responsibilities for that is not really related to a model or a view. Or an application might require to reuse the same view component but with different data processors. A controller can help with it here. In order to make it work the following conditions must be met:

- a model must store the data as raw as possible and must not apply any additional processing functions by reading it;
- a view must contain the only functionality, that are related for the visual representation, i.e. it must contain the smallest amount of data processors.

Under this conditions a controller between them can be easily inserted - a view must use a controller interface to get and display data, or use it to convert into raw format and set into the model..

```js
class TextController {
	...
}

mvcPack(
	SomeModelClass,
	{
		text: vcPack(TextView, TextController)
	}
);
```

```js
class CustomController {
	...
}

mvcPack(
	SomeModelClass,
	{
		customView(model) {
			const view = new CustomView(model, 'other arguments');
			const controller = new CustomController(model, 'other arguments');
			view.setController(controller);
			return view;
		}
	}
);
```


## Links

- [API](https://valerii-zinchenko.gitlab.io/mvc-pack/)
